# Docker + Ansible или Контейнерицазия + Управление контейнерами

## Введение

Все скрипты должны запускаться из текущей директории

## ANSIBLE

Управляем контейнерами при помощи инструмента Ansible  

```
# Отключаем проверку ключей SSH если подключение чисто по паролю и имени(или в конфиге ansible)
export ANSIBLE_HOST_KEY_CHECKING=False

# Пингуем сервера
ansible all -m ping -i ./ansible/hosts
```


## DOCKER 

Контейнеризация окружения при помощи инструмента Docker  

```
#mysql
docker run --restart=always --name mariadb -v "$PWD"/mysql:/var/lib/mysql -v "$PWD"/mysql/conf.d:/etc/mysql/conf.d -e MYSQL_ROOT_PASSWORD=root123321 -d mariadb:latest

#phpmyadmin
docker run --restart=always --name phpmyadmin -d --link mariadb:db -p 8080:80 phpmyadmin/phpmyadmin

#php-fpm
docker build -t artskif/php ./php-fpm
docker run -d --restart=always --name phpfpm --link mariadb:mysqlip -v "$PWD"/www:/var/www -w /var/www artskif/php

#sites
docker build -t artskif/site1 ./www/site1
docker build -t artskif/site2 ./www/site2
docker run -d --restart=always --name site1 --link mariadb:mysqlip --link phpfpm:phpfpm -v "$PWD"/www/site1/html:/var/www artskif/site1
docker run -d --restart=always --name site2 --link mariadb:mysqlip --link phpfpm:phpfpm -v "$PWD"/www/site2/html:/var/www artskif/site2

#proxy
docker build -t artskif/balancer ./balancer
docker run -d --name balancer --link site1:site1 --link site2:site2 -p 80:80 artskif/balancer
```